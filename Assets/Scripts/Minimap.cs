﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour {

	// ************************************************************************
	//	--- VARIABLES ---
	// ************************************************************************
	public Transform target;

	// ************************************************************************
	//	--- UNITY BASE METHODS ---
	// ************************************************************************

	void LateUpdate() {
		transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);
		// transform.rotation = Quaternion.Euler( 90f, target.eulerAngles.y, 0f ) ;
	}
}
