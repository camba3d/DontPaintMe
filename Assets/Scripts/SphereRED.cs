﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class SphereRED : MonoBehaviour {

    // ************************************************************************
    //	--- VARIABLES ---
    // ************************************************************************

    // Serializables
    float speed;
    float lifeTime;
    float maxDistance;

    // ************************************************************************
    //	--- UNITY BASE METHODS ---
    // ************************************************************************

    void Awake() {
        int dif = PlayerPrefs.GetInt("Difficulty");
        switch (dif) {
            case 1:
                InitVars(9f, 0.5f, 4.5f);
                break;
            case 2:
                InitVars(8f, 0.45f, 4f);
                break;
            default:
                InitVars(10f, 0.55f, 5f);
                break;
        }
    }

    void Start() {
        AudioManager.instance.Play("RedPunch");
        transform.position += transform.forward * 0.33f;
        transform.localScale = Vector3.zero;
        Destroy(gameObject, lifeTime);
    }

    void Update() {
        var tgt = FindTarget();
        if (!tgt.Equals(Vector3.zero)) {
            tgt = new Vector3(tgt.x, 1f, tgt.z);
            transform.position = Vector3.Lerp(transform.position, tgt, speed * Time.deltaTime);
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, speed * Time.deltaTime);
        } else {
            transform.position += transform.forward * speed * Time.deltaTime;
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, speed * Time.deltaTime);
            // Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "NPC") {
            other.gameObject.SendMessage("Hitted", "red");
        }
        if (other.tag != "Player")
            Destroy(gameObject, 0.2f);
    }

    // ************************************************************************
    //	--- HELPERS ---
    // ************************************************************************

    // Find the nearest NPC
    Vector3 FindTarget() {
        var v = Vector3.zero;
        var npcs = GameObject.FindGameObjectsWithTag("NPC");
        if (npcs.Length > 0) {
            foreach (var npc in npcs) {
                var dist = (transform.position - npc.transform.position).magnitude;
                if (dist < maxDistance) {
                    maxDistance = dist;
                    v = npc.transform.position;
                }
            }
        }
        return v;
    }

    // Varaibles initializer
    void InitVars(float speed, float lifeTime, float maxDistance) {
        this.speed = speed;
        this.lifeTime = lifeTime;
        this.maxDistance = maxDistance;
    }

}
