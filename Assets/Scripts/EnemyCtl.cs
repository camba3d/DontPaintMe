﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyCtl : MonoBehaviour {

	// ************************************************************************
	//	--- VARIABLES ---
	// ************************************************************************

	// From editor
	public GameObject brush;

	// Serializable
	float animSpeed;
	float agentSpeed;
	float throwPauseTime;

	// Backend
	bool brushThrown = true;

	// Unity objects
	Animator animCtl;
	NavMeshAgent agent;

	// ************************************************************************
	//	--- UNITY BASE METHODS ---
	// ************************************************************************

	void Awake() {
		int dif = PlayerPrefs.GetInt("Difficulty");
		switch (dif) {
			case 1:
				InitVars(3f, 6f, 0.4f);
				break;
			case 2:
				InitVars(3.2f, 6.5f, 0.3f);
				break;
			default:
				InitVars(2.8f, 5.5f, 0.6f);
				break;
		}
	}

	void Start() {

		// Agent init setup
		agent = GetComponent<NavMeshAgent>();
		agent.autoBraking = false;
		agent.autoRepath = true;

		// Animator int setup
		animCtl = GetComponent<Animator>();
	}

	void Update() {

		// Re-target direction and manage animations
		var newPos = GetRandPosition();
		var tgtReach = !agent.pathPending && agent.remainingDistance < 0.3f;
		if (newPos.Equals(Vector3.zero) && tgtReach) {
			animCtl.speed = 1;
			animCtl.Play("idle");
		} else {
			animCtl.Play("walk");
			animCtl.speed = animSpeed;
			agent.speed = agentSpeed;
			agent.destination = newPos;
		}

		// Avoid Y variations
		transform.position = new Vector3(transform.position.x, 0f, transform.position.z);

		// Try to paint a NPC
		var npcs = GameObject.FindGameObjectsWithTag("NPC");
		if (brushThrown && npcs.Length > 0) {
			StartCoroutine("ThrowBrush");
			brushThrown = !brushThrown;
		}

	}

	// ************************************************************************
	//	--- HELPERS ---
	// ************************************************************************

	// Generate a random position to change direction of this Enemy
	Vector3 GetRandPosition() {
		var v = Vector3.zero;
		var npcs = GameObject.FindGameObjectsWithTag("NPC");
		if (npcs.Length > 0) {
			var d = Mathf.Infinity;
			foreach (var npc in npcs) {
				var dist = (transform.position - npc.transform.position).magnitude;
				if (dist < d) {
					d = dist;
					v = npc.transform.position;
				}
			}
		}
		return v;
	}

	// Throw the paint sphere
	IEnumerator ThrowBrush() {
		var go = Instantiate(brush, transform) as GameObject;
		go.transform.parent = transform.parent;
		yield return new WaitForSeconds(throwPauseTime);
		brushThrown = true;
	}

	// Variables intializer
	void InitVars(float animSpeed, float agentSpeed, float throwPauseTime) {
		this.animSpeed = animSpeed;
		this.agentSpeed = agentSpeed;
		this.throwPauseTime = throwPauseTime;
	}

}
